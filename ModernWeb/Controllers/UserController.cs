using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Mvc;
using ModernWeb.Data;
using ModernWeb.Entity.Models;

namespace ModernWeb.Controllers
{
  [Produces("application/json")]
  [ODataRoutePrefix("User")]
  public class UserController : ODataEntityController<User>
  {

    [EnableQuery]
    public IActionResult Get() => Ok(DbSet());

    [EnableQuery]
    [ODataRoute("({key})")]
    public IActionResult Get([FromODataUri] Guid key) => Ok(DbSet().Where(_ => _.Id == key));


    [HttpPost]
    public async Task<IActionResult> Post([FromBody]User user)
    {
      DbSet().Attach(user);
      await SaveChangesAsync();
      return Ok(user);
    }

    [HttpPut]
    [ODataRoute("")]
    public async Task<IActionResult> Put([FromBody]User user)
    {
      DbSet().Attach(user);
      DbSet().Update(user);
      await SaveChangesAsync();
      return Ok(user);
    }

    [HttpDelete]
    [ODataRoute("({key})")]
    public async Task<IActionResult> Delete([FromODataUri] Guid key)
    {
      DbSet().RemoveRange(DbSet().Where(_ => _.Id == key));
      await SaveChangesAsync();
      return Ok();
    }
  }
}
