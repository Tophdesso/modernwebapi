using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.OData;
using Microsoft.AspNet.OData.Routing;
using Microsoft.AspNetCore.Mvc;
using ModernWeb.Data;
using ModernWeb.Entity.Models;

namespace ModernWeb.Controllers
{
  [Produces("application/json")]
  [ODataRoutePrefix("Role")]
  public class RoleController : ODataEntityController<Role>
  {
    [EnableQuery]
    public IActionResult Get() => Ok(DbSet());

    [EnableQuery]
    [ODataRoute("({key})")]
    public IActionResult Get([FromODataUri] Guid key) => Ok(DbSet().Where(_ => _.Id == key));

    [HttpPost]
    public async Task<IActionResult> Post([FromBody]Role role)
    {
      DbSet().Add(role);
      await SaveChangesAsync();
      return Ok(role);
    }

    [HttpPut]
    [ODataRoute("")]
    public async Task<IActionResult> Put([FromBody]Role role)
    {
      DbSet().Add(role);
      await SaveChangesAsync();
      return Ok(role);
    }

  }
}
