﻿using Microsoft.AspNet.OData.Builder;
using Microsoft.OData.Edm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ModernWeb.Entity.Models;
namespace ModernWeb.Data
{

    public static class ModernWebEDMConfig
    {
        public static IEdmModel GetEdmModel()
        {
            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();

            builder.EntitySet<User>(nameof(User))
                .EntityType
                .Expand()
                .Filter()
                .Count()
                .OrderBy()
                .Page()
                .Select();

            builder.EntitySet<Role>(nameof(Role))
                .EntityType
                .Expand()
                .Filter()
                .Count()
                .OrderBy()
                .Page()
                .Select();

            return builder.GetEdmModel();
        }

    }

}
