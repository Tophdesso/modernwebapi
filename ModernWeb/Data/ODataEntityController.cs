﻿using Microsoft.AspNet.OData;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ModernWeb.Entity.Models;
using Microsoft.AspNetCore.Http;

namespace ModernWeb.Data
{
    public class ODataEntityController<T> : ODataController where T : class, new()
    {
        protected ModernWebContext DbContext;
        protected DbSet<T> DbSet() => DbContext.Set<T>();

        protected async Task<int> SaveChangesAsync() => await DbContext.SaveChangesAsync();
        protected int SaveChanges() => DbContext.SaveChanges();

        public ODataEntityController()
        {
            DbContext = new ModernWebContext();
        }
    }
}
