﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModernWeb.Entity.Interfaces
{
    public interface IModernWebEntity
    {
        Guid Id { get; set; }
    }
}
