﻿using ModernWeb.Entity.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ModernWeb.Entity.Models
{
    public partial class User : IModernWebEntity
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual Role Role { get; set; }
    }
}
