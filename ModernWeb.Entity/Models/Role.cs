﻿using ModernWeb.Entity.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ModernWeb.Entity.Models
{
    public partial class Role : IModernWebEntity
    {
        public Role()
        {
            this.Users = new HashSet<User>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
